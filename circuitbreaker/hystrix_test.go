package circuitbreaker

import (
	"bitbucket.org/sellergrowth/common-go/log"
	"bytes"
	"context"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/h2non/gock.v1"
	"os"
	"testing"
)

var core = zapcore.NewCore(zapcore.NewConsoleEncoder(zap.NewDevelopmentEncoderConfig()), os.Stdout, zapcore.DebugLevel)
var zapLogger = zap.New(core)
var logger = log.NewFactory(zapLogger)
var tracer = opentracing.NoopTracer{}

func TestCallUsingCircuitBreaker_AllFails(t *testing.T) {
	var h = Hystrix{
		retries: 3,
		logger:  logger,
		tracer:  tracer,
	}

	defer gock.Off()
	hystrix.Flush()
	buildGockMatcherTimes(500, 4)

	res, err := h.CallUsingCircuitBreaker(context.TODO(), "TEST", "http://quotes-service", "GET")
	if err == nil {
		t.Error("Circuit break, err should not be nil")
	}
	if res != nil {
		t.Error("Circuit break, res should be nil")
	}
}

func TestCallUsingCircuitBreaker_LastSucceeds(t *testing.T) {
	var h = Hystrix{
		retries: 3,
		logger:  logger,
		tracer:  tracer,
	}

	defer gock.Off()
	buildGockMatcherTimes(500, 2)
	body := []byte("Some response")
	buildGockMatcherWithBody(200, string(body))
	hystrix.Flush()

	res, err := h.CallUsingCircuitBreaker(context.TODO(), "TEST", "http://quotes-service", "GET")
	if err != nil {
		t.Error("Circuit break, err should be nil")
	}
	if res == nil {
		t.Error("Circuit break, res should not be nil")
	}
	if bytes.Compare(res, body) != 0 {
		t.Errorf("Res: %s != %s", res, body)
	}
}

func TestCallUsingCircuitBreaker_OpensAfterThresholdPassed(t *testing.T) {
	var h = Hystrix{
		retries: 0,
		logger:  logger,
		tracer:  tracer,
	}

	defer gock.Off()
	for a := 0; a < 6; a++ {
		buildGockMatcher(500)
	}
	hystrix.Flush()

	hystrix.ConfigureCommand("TEST", hystrix.CommandConfig{
		RequestVolumeThreshold: 5,
	})

	for a := 0; a < 6; a++ {
		h.CallUsingCircuitBreaker(context.TODO(), "TEST", "http://quotes-service", "GET")
	}
	cb, _, _ := hystrix.GetCircuit("TEST")
	if cb.IsOpen() != true {
		t.Error("Circuit breaker shoud has been open")
	}
}

func buildGockMatcherTimes(status int, times int) {
	for a := 0; a < times; a++ {
		buildGockMatcher(status)
	}
}

func buildGockMatcherWithBody(status int, body string) {
	gock.New("http://quotes-service").
		Reply(status).BodyString(body)
}

func buildGockMatcher(status int) {
	buildGockMatcherWithBody(status, "")
}
