package circuitbreaker

import (
	"bitbucket.org/sellergrowth/common-go/log"
	"bitbucket.org/sellergrowth/common-go/tracing"
	"context"
	"fmt"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/eapache/go-resiliency/retrier"
	"github.com/opentracing-contrib/go-stdlib/nethttp"
	"github.com/opentracing/opentracing-go"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"
	"time"
)

type Hystrix struct {
	retries int
	logger  log.Factory
	tracer  opentracing.Tracer
}

// CallUsingCircuitBreaker performs a HTTP call inside a circuit breaker.
func (h Hystrix) CallUsingCircuitBreaker(ctx context.Context, breakerName string, url string, method string) ([]byte, error) {
	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		return nil, err
	}
	return h.PerformHTTPRequestCircuitBreaker(ctx, breakerName, req)
}

// PerformHTTPRequestCircuitBreaker performs the supplied http.Request within a circuit breaker.
func (h Hystrix) PerformHTTPRequestCircuitBreaker(ctx context.Context, breakerName string, req *http.Request) ([]byte, error) {
	h.logger = h.logger.With(
		zap.String("breaker", breakerName),
		zap.String("method", req.Method),
		zap.String("url", req.URL.String()))

	output := make(chan []byte, 1)
	errors := hystrix.Go(breakerName, func() error {
		ctx, span := tracing.NewSpan(ctx, h.tracer, "CALL IN BREAKER")
		defer span.Finish()
		err := h.callWithRetries(ctx, req, output, h.retries)
		return err // For hystrix, forward the err from the retrier. It's nil if OK.
	}, func(err error) error {
		h.logger.For(ctx).Error("Into fallback func", zap.Error(err))
		circuit, _, _ := hystrix.GetCircuit(breakerName)
		h.logger.For(ctx).Error(fmt.Sprintf("Circuit state is: %v", circuit.IsOpen()))
		return err
	})

	select {
	case out := <-output:
		h.logger.For(ctx).Debug("Call success in breaker")
		return out, nil

	case err := <-errors:
		h.logger.For(ctx).Debug("Got error in breaker", zap.Error(err))
		return nil, err
	}
}

func (h Hystrix) callWithRetries(ctx context.Context, req *http.Request, output chan []byte, retries int) error {

	r := retrier.New(retrier.ConstantBackoff(retries, 100*time.Millisecond), nil)
	attempt := 0
	err := r.Run(func() error {
		attempt++
		req = req.WithContext(ctx)
		req, ht := nethttp.TraceRequest(
			h.tracer, req,
			nethttp.OperationName(fmt.Sprintf("HTTP %s: %s", req.Method, req.URL.String())))
		defer ht.Finish()
		ctx = req.Context()

		resp, err := http.DefaultClient.Do(req)
		if err == nil && resp.StatusCode < 299 {
			responseBody, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				output <- responseBody
				return nil
			}
			return err
		} else if err == nil {
			err = fmt.Errorf("status was %v", resp.StatusCode)
		}
		h.logger.For(ctx).Error("Retry failed", zap.Int("attempt", attempt))
		return err
	})
	return err
}

// ConfigureHystrix sets up hystrix circuit breakers.
func ConfigureHystrix(commands []string) {

	for _, command := range commands {
		hystrix.ConfigureCommand(command, hystrix.CommandConfig{
			Timeout:                resolveProperty(command, "Timeout"),
			MaxConcurrentRequests:  resolveProperty(command, "MaxConcurrentRequests"),
			ErrorPercentThreshold:  resolveProperty(command, "ErrorPercentThreshold"),
			RequestVolumeThreshold: resolveProperty(command, "RequestVolumeThreshold"),
			SleepWindow:            resolveProperty(command, "SleepWindow"),
		})
		//logrus.Printf("Circuit %v settings: %v", command, hystrix.GetCircuitSettings()[command])
	}
}

func resolveProperty(command string, prop string) int {
	if viper.IsSet("hystrix.command." + command + "." + prop) {
		return viper.GetInt("hystrix.command." + command + "." + prop)
	}
	return getDefaultHystrixConfigPropertyValue(prop)

}

func getDefaultHystrixConfigPropertyValue(prop string) int {
	switch prop {
	case "Timeout":
		return hystrix.DefaultTimeout
	case "MaxConcurrentRequests":
		return hystrix.DefaultMaxConcurrent
	case "RequestVolumeThreshold":
		return hystrix.DefaultVolumeThreshold
	case "SleepWindow":
		return hystrix.DefaultSleepWindow
	case "ErrorPercentThreshold":
		return hystrix.DefaultErrorPercentThreshold
	}
	panic("Got unknown hystrix property: " + prop + ". Panicing!")
}
