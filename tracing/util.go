package tracing

import (
	"context"
	"github.com/opentracing/opentracing-go"
)

func NewSpan(ctx context.Context, tracer opentracing.Tracer, OpName string) (context.Context, opentracing.Span) {
	// retrieve current Span from Context
	var parentCtx opentracing.SpanContext
	parentSpan := opentracing.SpanFromContext(ctx)
	if parentSpan != nil {
		parentCtx = parentSpan.Context()
	}
	// start a new Span to wrap HTTP request
	span := tracer.StartSpan(
		OpName,
		opentracing.ChildOf(parentCtx),
	)
	// make the Span current in the context
	ctx = opentracing.ContextWithSpan(ctx, span)
	return ctx, span
}
