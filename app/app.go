package app

import (
	"bitbucket.org/sellergrowth/common-go/log"
	"bitbucket.org/sellergrowth/common-go/tracing"
	"context"
	"encoding/json"
	"fmt"
	"github.com/opentracing/opentracing-go"
	"github.com/spf13/viper"
	"github.com/uber/jaeger-client-go/config"
	"github.com/uber/jaeger-lib/metrics"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"
)

type Options struct {
	Logger           log.Factory
	ConfigServerURL  string
	ProjectName      string
	AppName          string
	Profile          string
	ConfigBranch     string
	JaegerTracerConf *config.Configuration
	metricsFactory   metrics.Factory
}

type httpClient struct {
	tracing.HTTPClient
}

type App struct {
	*Options

	Config *viper.Viper
	tracer opentracing.Tracer
	mux    *tracing.TracedServeMux
}

type Option func(o *Options)

func MetricsFactory(metricsFactory metrics.Factory) Option {
	return func(o *Options) {
		o.metricsFactory = metricsFactory
	}
}

func InitApp(o Options, opts ...Option) *App {
	for _, opt := range opts {
		opt(&o)
	}

	app := &App{Options: &o}
	if app.Config == nil {
		app.Config = viper.New()
	}

	o.Logger = o.Logger.With(zap.String("service", o.AppName))
	app.tracer = tracing.Init(o.AppName, o.metricsFactory, o.Logger, app.JaegerTracerConf)
	app.mux = tracing.NewServeMux(app.tracer)

	return app
}

func (a *App) LoadAppConfiguration() {
	a.LoadConfiguration(a.ConfigServerURL, a.AppName, a.Profile, a.ConfigBranch, true)
}

func (a *App) LoadProjectConfiguration(panicOnFail bool) {
	a.LoadConfiguration(a.ConfigServerURL, a.ProjectName, a.Profile, a.ConfigBranch, panicOnFail)
}

func (a *App) LoadConfiguration(configServerURL string, name string, profile string, branch string, panicOnFail bool) {
	url := fmt.Sprintf("%s/%s/%s/%s", configServerURL, name, profile, branch)
	a.Logger.Bg().Info("loading Config", zap.String("url", url))
	body, err := a.fetchConfiguration(url)
	if err != nil {
		if panicOnFail {
			a.Logger.Bg().Error("couldn't load configuration",
				zap.Error(err))
			panic("Couldn't load configuration, cannot start. Terminating. Error: " + err.Error())
		}
	}
	a.parseConfiguration(body)
}

func (a *App) NewSpan(ctx context.Context, OpName string) (context.Context, opentracing.Span) {
	return tracing.NewSpan(ctx, a.tracer, OpName)
}

func (a *App) ForClient(client *http.Client) *httpClient {
	return &httpClient{tracing.HTTPClient{
		Tracer: a.tracer,
		Client: client,
	}}
}

func (a *App) Handle(pattern string, handler http.Handler) {
	a.mux.Handle(pattern, handler)
}

func (a *App) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	a.mux.ServeHTTP(w, r)
}

func (a *App) fetchConfiguration(url string) ([]byte, error) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()
	a.Logger.Bg().Info("getting Config", zap.String("url", url))
	resp, err := http.Get(url)
	if err != nil || resp.StatusCode != 200 {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, err
}

func (a *App) parseConfiguration(body []byte) {
	var cloudConfig springCloudConfig
	err := json.Unmarshal(body, &cloudConfig)
	if err != nil {
		return
	}

	for key, value := range cloudConfig.PropertySources[0].Source {
		a.Config.Set(key, value)
		a.Logger.Bg().Info("loading Config property", zap.Any(key, value))
	}
}

type springCloudConfig struct {
	Name            string           `json:"name"`
	Profiles        []string         `json:"profiles"`
	Label           string           `json:"label"`
	Version         string           `json:"version"`
	PropertySources []propertySource `json:"propertySources"`
}

type propertySource struct {
	Name   string                 `json:"name"`
	Source map[string]interface{} `json:"source"`
}
