package app

import (
	zapLog "bitbucket.org/sellergrowth/common-go/log"
	"context"
	"encoding/json"
	"github.com/uber/jaeger-client-go/config"
	"go.uber.org/zap"
	"gopkg.in/h2non/gock.v1"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strings"
	"testing"
)

var logger, _ = zap.NewDevelopment()
var logFactory = zapLog.NewFactory(logger.With(zap.String("test", "test")))

var conf = springCloudConfig{
	Name:     "test",
	Profiles: []string{"dev"},
	PropertySources: []propertySource{
		{Name: "file:/Config-repo/test-dev.yml", Source: map[string]interface{}{"server_port": "6666", "server_name": "test"}},
	},
}

func TestApp_LoadAppConfiguration(t *testing.T) {
	testApp_LoadAppConfiguration(t, &conf)
}

func TestApp_ServeHTTP(t *testing.T) {
	ln := newLocalListener(t)
	addr := ln.Addr().String()
	port := strings.Split(addr, ":")[1]
	ln.Close()

	var conf = springCloudConfig{
		Name:     "test",
		Profiles: []string{"dev"},
		PropertySources: []propertySource{
			{Name: "file:/Config-repo/test-dev.yml", Source: map[string]interface{}{"server_port": port, "server_name": "test"}},
		},
	}

	app := testApp_LoadAppConfiguration(t, &conf)

	var respBody = "home"
	app.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		app.Logger.For(r.Context()).Info("handle request", zap.String("url", "/"))
		w.Write([]byte(respBody))
	}))

	var srv http.Server
	srv = http.Server{Addr: addr, Handler: app}
	go func() {
		srv.ListenAndServe()
	}()
	defer srv.Shutdown(context.Background())
	res, err := http.Get("http://" + addr)
	if err != nil {
		t.Errorf("Request http://%s err: %v", addr, err)
	}
	responseBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Read body err: %v", err)
	}
	if string(responseBody) != respBody {
		t.Errorf("Resp body: %s != %s", responseBody, respBody)
	}
}

func testApp_LoadAppConfiguration(t *testing.T, conf *springCloudConfig) *App {
	data, err := json.Marshal(*conf)
	if err != nil {
		log.Fatalf("Marshal conf err: %v", err)
	}
	confJson := string(data)

	defer gock.Off()
	gock.New("http://configserver:8888/accountservice-dev/dev/master").
		Reply(200).BodyString(confJson)

	appConf := Options{
		Logger:          logFactory,
		ConfigServerURL: "http://configserver:8888",
		AppName:         "accountservice-dev",
		ProjectName:     "projectName",
		Profile:         "dev",
		ConfigBranch:    "master",
		JaegerTracerConf: &config.Configuration{
			Disabled: true,
		}}
	app := InitApp(appConf)
	app.LoadProjectConfiguration(false)
	app.LoadAppConfiguration()

	for k, v := range conf.PropertySources[0].Source {
		if app.Config.Get(k) != v {
			t.Errorf("Config for k: %s != v: %v", k, v)
		}
	}

	return app
}

func newLocalListener(t *testing.T) net.Listener {
	ln, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		ln, err = net.Listen("tcp6", "[::1]:0")
	}
	if err != nil {
		t.Fatal(err)
	}
	return ln
}
