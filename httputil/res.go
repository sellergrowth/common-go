package httputil

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type ResWriter interface {
	WriteJson(http.ResponseWriter)
}

type Message map[string]string

type res struct {
	Data    interface{} `json:"data"`
	Code    string      `json:"code"`
	Message Message     `json:"message"`
	Version float32     `json:"version"`
}

func HandleHealth(w http.ResponseWriter, req *http.Request) {
	res := Success(nil)
	res.WriteJson(w)
}

func (r *res) WriteJson(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	if err := r.writeJson(w); err != nil {
		msg := fmt.Sprintf("WriteJson err: %v res: %+v", err, *r)
		http.Error(w, msg, http.StatusInternalServerError)
	}
}

func (r *res) writeJson(w io.Writer) error {
	enc := json.NewEncoder(w)
	enc.SetEscapeHTML(false)
	return enc.Encode(*r)
}

type successRes struct {
	res
}

type invalidRes struct {
	res
}

type logicInfoRes struct {
	res
}

type logicErrorRes struct {
	res
}

type emptyInvalidRes struct{}

func success(data interface{}, msg string) *successRes {
	return &successRes{res{
		Data:    data,
		Code:    "ok",
		Message: Message{"ok": msg},
		Version: 2.0,
	}}
}

// Success return a well formed success result for data.
// The message is {"ok":"success"}
func Success(data interface{}) *successRes {
	return success(data, "success")
}

// Success return a well formed success result for data.
// The message is {"ok":msg}, where msg is param
func SuccessWithMsg(data interface{}, msg string) *successRes {
	return success(data, msg)
}

// EmptyInvalid return an empty invalid result.
// The result can't by used directly, should use AddMessage to get
// a nonempty result
func EmptyInvalid() *emptyInvalidRes {
	return &emptyInvalidRes{}
}

func (r *emptyInvalidRes) AddMessage(code string, errMsg string) *invalidRes {
	return Invalid(Message{code: errMsg})
}

// Invalid return an invalid result with message.
func Invalid(message Message) *invalidRes {
	return &invalidRes{res{
		Data:    nil,
		Code:    "validate",
		Message: message,
		Version: 2.0,
	}}
}

func (r *invalidRes) AddMessage(code string, errMsg string) *invalidRes {
	r.Message[code] = errMsg
	return r
}

// LogicInfo return a well formed result for logic info.
func LogicInfo(code string, errMsg string) *logicInfoRes {
	return &logicInfoRes{res{
		Data: nil,
		Code: code,
		Message: Message{
			code: errMsg,
		},
		Version: 2.0,
	}}
}

// LogicError return a well formed result for logic error.
func LogicError(code string, errMsg string) *logicErrorRes {
	return &logicErrorRes{res{
		Data: nil,
		Code: code,
		Message: Message{
			code: errMsg,
		},
		Version: 2.0,
	}}
}
