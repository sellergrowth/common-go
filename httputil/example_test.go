package httputil

import (
	"fmt"
	"net/http"
	"strings"
	"time"
)

type Keyword string

type Datetime time.Time

func (d *Datetime) UnmarshalJSON(b []byte) error {
	const datetimeLayout = "2006-01-02 15:04:05"
	value := fmt.Sprintf("%s", b)
	value = strings.Trim(value, `"`)
	t, err := time.Parse(datetimeLayout, value)
	if err != nil {
		return err
	}
	*d = Datetime(t)
	return nil
}

func (d Datetime) MarshalJSON() ([]byte, error) {
	return time.Time(d).MarshalJSON()
}

type Completion struct {
	Seed       Keyword   `json:"seed"`
	Completion []Keyword `json:"completion"`
	GmtCreate  Datetime  `json:"gmt_create"`
}

type SearchResult struct {
	TookInMillis int64        `json:"took"`
	Results      []Completion `json:"results"`
	Total        int64        `json:"total"`
	From         int          `json:"from"`
	Size         int          `json:"size"`
}

func ExampleSuccess() {
	http.HandleFunc("/success", func(w http.ResponseWriter, r *http.Request) {

		completions := []Completion{
			{Seed: "switch", Completion: []Keyword{"switch case", "nintendo switch"}, GmtCreate: Datetime(time.Date(2018, 2, 14, 0, 0, 0, 0, time.UTC))},
			{Seed: "switch case", Completion: []Keyword{"switch case xx", "nintendo switch case"}, GmtCreate: Datetime(time.Date(2018, 2, 14, 0, 0, 0, 0, time.UTC))},
		}

		searchResult := SearchResult{
			TookInMillis: 100,
			Results:      completions,
			Total:        2, From: 0, Size: 20,
		}

		res := Success(searchResult)
		res.WriteJson(w)
		return
	})
}

func ExampleSuccessWithMsg() {
	http.HandleFunc("/success", func(w http.ResponseWriter, r *http.Request) {

		completions := []Completion{
			{Seed: "switch", Completion: []Keyword{"switch case", "nintendo switch"}, GmtCreate: Datetime(time.Date(2018, 2, 14, 0, 0, 0, 0, time.UTC))},
			{Seed: "switch case", Completion: []Keyword{"switch case xx", "nintendo switch case"}, GmtCreate: Datetime(time.Date(2018, 2, 14, 0, 0, 0, 0, time.UTC))},
		}

		searchResult := SearchResult{
			TookInMillis: 100,
			Results:      completions,
			Total:        2, From: 0, Size: 20,
		}

		res := SuccessWithMsg(searchResult, "Get search result success")
		res.WriteJson(w)
		return
	})
}

func ExampleEmptyInvalid() {
	http.HandleFunc("/invalid", func(w http.ResponseWriter, r *http.Request) {
		emptyInvalid := EmptyInvalid()
		res := emptyInvalid.AddMessage("username", "用户名错误。")
		res.AddMessage("phone", "手机号格式错误。")
		res.WriteJson(w)
		return
	})
}

func ExampleInvalid() {
	http.HandleFunc("/invalid", func(w http.ResponseWriter, r *http.Request) {
		res := Invalid(Message{
			"username": "用户名错误。",
		})
		res.WriteJson(w)
		return
	})
}

func ExampleLogicInfo() {
	http.HandleFunc("/logicInfo", func(w http.ResponseWriter, r *http.Request) {
		res := LogicInfo("info:user_not_exist", "用户不存在。")
		res.WriteJson(w)
		return
	})
}

func ExampleLogicError() {
	http.HandleFunc("/logicError", func(w http.ResponseWriter, r *http.Request) {
		res := LogicError("error:rpc_error", "Rpc未知错误。")
		res.WriteJson(w)
		return
	})
}
