package httputil

import (
	"bytes"
	"testing"
	"time"
)

func TestSuccess(t *testing.T) {
	completions := []Completion{
		{Seed: "switch", Completion: []Keyword{"switch case", "nintendo switch"}, GmtCreate: Datetime(time.Date(2018, 2, 14, 0, 0, 0, 0, time.UTC))},
		{Seed: "switch case", Completion: []Keyword{"switch case xx", "nintendo switch case"}, GmtCreate: Datetime(time.Date(2018, 2, 14, 0, 0, 0, 0, time.UTC))},
	}

	searchResult := SearchResult{
		TookInMillis: 100,
		Results:      completions,
		Total:        2, From: 0, Size: 20,
	}

	res := Success(searchResult)

	var b bytes.Buffer
	result := []byte(`{"data":{"took":100,"results":[{"seed":"switch","completion":["switch case","nintendo switch"],"gmt_create":"2018-02-14T00:00:00Z"},{"seed":"switch case","completion":["switch case xx","nintendo switch case"],"gmt_create":"2018-02-14T00:00:00Z"}],"total":2,"from":0,"size":20},"code":"ok","message":{"ok":"success"},"version":2}
`)

	err := res.writeJson(&b)
	if err != nil {
		t.Errorf("WriteJson err: %v", err)
	}

	if bytes.Compare(b.Bytes(), result) != 0 {
		t.Errorf("Result given %s != %s", b.Bytes(), result)
	}
}

func TestSuccessWithMsg(t *testing.T) {
	completions := []Completion{
		{Seed: "switch", Completion: []Keyword{"switch case", "nintendo switch"}, GmtCreate: Datetime(time.Date(2018, 2, 14, 0, 0, 0, 0, time.UTC))},
		{Seed: "switch case", Completion: []Keyword{"switch case xx", "nintendo switch case"}, GmtCreate: Datetime(time.Date(2018, 2, 14, 0, 0, 0, 0, time.UTC))},
	}

	searchResult := SearchResult{
		TookInMillis: 100,
		Results:      completions,
		Total:        2, From: 0, Size: 20,
	}

	res := SuccessWithMsg(searchResult, "Get search result success")

	var b bytes.Buffer
	result := []byte(`{"data":{"took":100,"results":[{"seed":"switch","completion":["switch case","nintendo switch"],"gmt_create":"2018-02-14T00:00:00Z"},{"seed":"switch case","completion":["switch case xx","nintendo switch case"],"gmt_create":"2018-02-14T00:00:00Z"}],"total":2,"from":0,"size":20},"code":"ok","message":{"ok":"Get search result success"},"version":2}
`)
	err := res.writeJson(&b)
	if err != nil {
		t.Errorf("WriteJson err: %v", err)
	}
	if bytes.Compare(b.Bytes(), result) != 0 {
		t.Errorf("Result given %s != %s", b.Bytes(), result)
	}
}

func TestEmptyInvalid(t *testing.T) {
	res := EmptyInvalid()
	iRes := res.AddMessage("username", "用户名错误。")
	iRes.AddMessage("phone", "手机号格式错误。")
	var b bytes.Buffer
	result := []byte(`{"data":null,"code":"validate","message":{"phone":"手机号格式错误。","username":"用户名错误。"},"version":2}
`)
	err := iRes.writeJson(&b)
	if err != nil {
		t.Errorf("WriteJson err: %v", err)
	}
	if bytes.Compare(b.Bytes(), result) != 0 {
		t.Errorf("Result given %s != %s", b.Bytes(), result)
	}
}

func TestInvalid2(t *testing.T) {
	res := Invalid(Message{
		"username": "用户名错误。",
	})
	res.AddMessage("phone", "手机号格式错误。")
	var b bytes.Buffer
	result := []byte(`{"data":null,"code":"validate","message":{"phone":"手机号格式错误。","username":"用户名错误。"},"version":2}
`)
	err := res.writeJson(&b)
	if err != nil {
		t.Errorf("WriteJson err: %v", err)
	}
	if bytes.Compare(b.Bytes(), result) != 0 {
		t.Errorf("Result given %s != %s", b.Bytes(), result)
	}
}

func TestLogicInfo(t *testing.T) {
	res := LogicInfo("info:user_not_exist", "用户不存在。")
	var b bytes.Buffer
	result := []byte(`{"data":null,"code":"info:user_not_exist","message":{"info:user_not_exist":"用户不存在。"},"version":2}
`)

	err := res.writeJson(&b)
	if err != nil {
		t.Errorf("WriteJson err: %v", err)
	}
	if bytes.Compare(b.Bytes(), result) != 0 {
		t.Errorf("Result given %s != %s", b.Bytes(), result)
	}
}

func TestLogicError(t *testing.T) {
	res := LogicError("error:rpc_error", "Rpc未知错误。")
	var b bytes.Buffer
	result := []byte(`{"data":null,"code":"error:rpc_error","message":{"error:rpc_error":"Rpc未知错误。"},"version":2}
`)

	err := res.writeJson(&b)
	if err != nil {
		t.Errorf("WriteJson err: %v", err)
	}
	if bytes.Compare(b.Bytes(), result) != 0 {
		t.Errorf("Result given %s != %s", b.Bytes(), result)
	}
}
